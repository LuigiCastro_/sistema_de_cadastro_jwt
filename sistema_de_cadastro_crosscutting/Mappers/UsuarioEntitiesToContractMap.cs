using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_domain_contracts;
using AutoMapper;

namespace sistema_de_cadastro_crosscutting_mappers;
public class UsuarioEntitiesToContractMap : Profile
{
    
    // CRIANDO AUTOMAPPER ENTRE ENTITY E REQUEST/RESPONSE
    public UsuarioEntitiesToContractMap()
    {
        CreateMap<UsuarioEntities, UsuarioRequest>().ReverseMap();
        CreateMap<UsuarioEntities, UsuarioResponse>().ReverseMap();
        CreateMap<UsuarioEntities, UsuarioCadastroRequest>().ReverseMap();
        CreateMap<UsuarioCadastroRequest, UsuarioRequest>().ReverseMap();
        CreateMap<UsuarioCadastroRequest, UsuarioResponse>().ReverseMap();
        
        // CreateMap<UsuarioEntities, UsuarioDto>().ReverseMap();
        // CreateMap<UsuarioDto, UsuarioEntities>().ReverseMap();
        // CreateMap<UsuarioDto, UsuarioRequest>().ReverseMap();
        // CreateMap<UsuarioDto, UsuarioResponse>().ReverseMap();
        // CreateMap<UsuarioDto, UsuarioCadastroRequest>().ReverseMap();
    }    
}