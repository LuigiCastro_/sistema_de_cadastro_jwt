using Microsoft.EntityFrameworkCore.Metadata.Builders;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository_mappings;

public class UsuarioEntitiesMap : IEntityTypeConfiguration<UsuarioEntities>
{
    public void Configure(EntityTypeBuilder<UsuarioEntities> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).HasColumnName("id");
        builder.Property(x => x.Ativo).HasColumnName("ativo");
        builder.Property(x => x.DataCriaçao).HasColumnName("dataCriaçao");
        builder.Property(x => x.DataAlteraçao).HasColumnName("dataAlteraçao");
        builder.Property(x => x.Nome).HasColumnName("nome");
        builder.Property(x => x.Telefone).HasColumnName("telefone");
        builder.Property(x => x.Email).HasColumnName("email");
        builder.Property(x => x.Senha).HasColumnName("senha");
        builder.Property(x => x.CPF).HasColumnName("cpf");
        builder.Property(x => x.DataNascimento).HasColumnName("dataNascimento");
        builder.Property(x => x.EndereçoId).HasColumnName("endereçoId");
        // builder.Property(x => x.Permissao).HasColumnName("permissao");
        builder.ToTable("usuarios");

        // Defifnindo uma propriedade como opcional.
        // builder.Property(x => x.Telefone).IsRequired(false);
    }
}

