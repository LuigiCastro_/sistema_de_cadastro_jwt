namespace sistema_de_cadastro_domain_entities
{
    public class EmpresaEntities : BaseEntities
    {
        public string Nome {get; set;}
        public string NomeFantasia {get; set;}
        public string Cnpj {get; set;}
        public int EndereçoId {get; set;}
        public EndereçoEntities Endereço {get; set;}
    }
}