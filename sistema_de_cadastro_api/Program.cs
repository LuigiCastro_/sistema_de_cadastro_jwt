using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using sistema_de_cadastro_ioc;
using System.Reflection;
using System.Text;


var builder = WebApplication.CreateBuilder(args);

// "DefaultConnection": "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
// "DefaultConnection": "server=localhost; database=master; integrated Security=True"
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

NativeInjectorBootStrapper.RegisterAppDependencies(builder.Services);
NativeInjectorBootStrapper.RegisterAppDependenciesContext(builder.Services, connectionString);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();



// CONFIGURANDO SWAGGER
builder.Services.AddSwaggerGen(swagger => 
{
    swagger.SwaggerDoc("v1",
                        new Microsoft.OpenApi.Models.OpenApiInfo
                        {
                            Title = "Descriçao_CRUD",
                            Version = "v1",
                            Contact = new Microsoft.OpenApi.Models.OpenApiContact {Name = "Luigi"},
                            TermsOfService = new Uri("http://raroacademy.com")
                        });
    
    // var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlFile = @"C:\Users\luigi\ProjetoTECH\sistema_de_cadastro_jwt\sistema_de_cadastro_api\sistema_de_cadastro_jwt.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    swagger.IncludeXmlComments(xmlFile);
});




var _secretKey = builder.Configuration["SecretKey"]; // CRIPTOGRAFA UMA SECRETKEY ?
var secretKey = Encoding.ASCII.GetBytes(_secretKey); // EMBARALHA A SECRETKEY PARA DIFICULTAR...
// JWT : JSON WEB TOKEN [JWT.IO]
// CONFIGURANDO O JWT. Objetivo : criptografar a segurança do sistema.
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(option =>
{
    option.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(secretKey),
        ValidateIssuer = false,
        ValidateAudience = false
    };
});




var app = builder.Build();

// Configure the HTTP request pipeline.
// É NECESSÁRIO CONFIGURAR O USO DO SWAGGER TBM, POIS O BUILDER.BUILD SÓ BUSCA RECURSOS INTERNOS.
if (app.Environment.IsDevelopment()) // Pelo fato do Enviroment estar em Development, é possível acessar o Swagger. Caso estivesse em Production, não seria possível.
{
    app.UseSwagger();
    app.UseSwaggerUI(swagger => 
    {
        swagger.SwaggerEndpoint("/swagger/v1/swagger.json", "sistema_de_cadastro_jwt");
    }); // Além de ver o resultado no swagger, ver em json tbm.
}




// DEPOIS DE CONFIGURADO O JWT AUTHENTICATION, ATIVAR O USO:
app.UseAuthentication();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


// # CONNECTIONSTRING
// PROGRAM
// APPSETTINGS
// CONFIGURE REPOSITORY
// NATIVE (REPOSITORY)