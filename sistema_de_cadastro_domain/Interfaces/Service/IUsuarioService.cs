using sistema_de_cadastro_domain_contracts;

namespace sistema_de_cadastro_domain_interfaces;
public interface IUsuarioService : IBaseCRUD<UsuarioRequest, UsuarioResponse>
{
    Task <UsuarioResponse> PostCadastroRequest(UsuarioCadastroRequest usuarioRequest);
    Task<UsuarioResponse> GetByCpf(string cpf);
    Task<IEnumerable<UsuarioDto>> GetWithoutCpf();

}