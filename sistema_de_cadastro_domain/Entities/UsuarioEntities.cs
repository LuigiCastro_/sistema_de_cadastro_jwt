using sistema_de_cadastro_domain_enums;

namespace sistema_de_cadastro_domain_entities;
public class UsuarioEntities : BaseEntities
{
    public string Nome {get; set;}
    public string? Telefone {get; set;}
    public string Email {get; set;}
    public string Senha {get; set;}
    public string CPF {get; set;}
    public string DataNascimento {get; set;}            // TRNASFORMAR EM DATETIME
    public int EndereçoId {get; set;}
    public EndereçoEntities Endereço {get; set;}
    public string Permissao {get; set;}
}