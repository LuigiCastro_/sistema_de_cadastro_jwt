using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using System.Text.RegularExpressions;

namespace sistema_de_cadastro_service_validator;

public class ServiceValidator : IServiceValidator
{
    public static Regex rgxNome = new Regex(@"(?<Nome>[a-zA-Z])");
    public static Regex rgxTelefone = new Regex(@"^\(?[1-9]{2}\)? ?(?:[2-8]|9[1-9])[0-9]{3}\-?[0-9]{4}$");
    public static Regex rgxEmail = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
    public static Regex rgxCpf = new Regex(@"^\d{3}\d{3}\d{3}\d{2}$");
    public static Regex rgxDataNasimento = new Regex(@"(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[0-2])/((1[2-9]|[2-9][0-9])[0-9]{2})");
    public static Regex rgxSenha = new Regex(@"((?=.*\d)(?=.*[A-Z]).{8,50})");

    public void ValidarCadastroUsuario(UsuarioCadastroRequest usuarioRequest)
    {
        if(usuarioRequest == null)
        {
            throw new Exception("Objeto não pode ser nulo");
        }
        if(usuarioRequest.Nome == null || !rgxNome.IsMatch(usuarioRequest.Nome))
        {
            throw new Exception("Nome inválido");
        }
        if(usuarioRequest.Telefone == null || !rgxTelefone.IsMatch(usuarioRequest.Telefone))
        {
            throw new Exception("Telefone inválido");
        }
        if(usuarioRequest.Email == null || !rgxEmail.IsMatch(usuarioRequest.Email))
        {
            throw new Exception("Email inválido");
        }
        if(usuarioRequest.Senha == null || !rgxSenha.IsMatch(usuarioRequest.Senha))
        {
            throw new Exception("Senha inválida");
        }
        if(usuarioRequest.CPF == null || !rgxCpf.IsMatch(usuarioRequest.CPF))
        {
            throw new Exception("CPF inválido");
        }
        if(usuarioRequest.DataNascimento == null || !rgxDataNasimento.IsMatch(usuarioRequest.DataNascimento))
        {
            throw new Exception("Data de nascimento inválida");
        }
        if(usuarioRequest.Permissao == null || usuarioRequest.Permissao != "Administrador")
        {
            if(usuarioRequest.Permissao == null || usuarioRequest.Permissao != "Cliente")
            {
                throw new Exception("Permissão inválida");
            }
        }
    }
}