using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_domain_enums;
using sistema_de_cadastro_domain_shared;
using sistema_de_cadastro_service_validator;
using System.Text.RegularExpressions;
using AutoMapper;


namespace sistema_de_cadastro_service;
public class UsuarioService : IUsuarioService
{
    private readonly IUsuarioRepository _usuarioRepository;
    private readonly IMapper _mapper;
    private readonly IServiceValidator _serviceValidator;
    public UsuarioService(IUsuarioRepository usuarioRepository, IMapper mapper)
    {
        _usuarioRepository = usuarioRepository;
        _mapper = mapper;
    }

    
    
    

    public async Task <UsuarioResponse> Post(UsuarioRequest usuarioRequest)
    {
        throw new NotImplementedException();
    }

    public async Task <UsuarioResponse> PostCadastroRequest(UsuarioCadastroRequest usuarioRequest)
    {
        _serviceValidator.ValidarCadastroUsuario(usuarioRequest);

        var requestUsuarioEntities = _mapper.Map<UsuarioEntities>(usuarioRequest);
        requestUsuarioEntities.Senha = Cryptography.Encrypt(usuarioRequest.Senha);
        var usuarioCadastrado = await _usuarioRepository.Post(requestUsuarioEntities);
        return _mapper.Map<UsuarioResponse>(usuarioCadastrado);
    }
    public async Task<IEnumerable<UsuarioResponse>> Get()
    {
        var listaUsuariosDatabase = await _usuarioRepository.Get();
        return _mapper.Map<IEnumerable<UsuarioResponse>>(listaUsuariosDatabase);
    }

    public async Task<IEnumerable<UsuarioDto>> GetWithoutCpf()
    {
        var listaUsuariosDatabase = await _usuarioRepository.GetWithoutCpf();
        return _mapper.Map<IEnumerable<UsuarioDto>>(listaUsuariosDatabase);
    }


    public async Task<UsuarioResponse> GetById(int id) 
    {
        var usuarioEncontrado = await _usuarioRepository.GetById(id);
        return _mapper.Map<UsuarioResponse>(usuarioEncontrado);
    }
    public async Task<UsuarioResponse> GetByCpf(string cpf)
    {
        var usuarioEncontrado = await _usuarioRepository.GetByCpf(cpf);
        return _mapper.Map<UsuarioResponse>(usuarioEncontrado);
    }
    public async Task<UsuarioResponse> Put(UsuarioRequest usuarioRequest, int? id) 
    {
        // var requestUsuarioEntities = _mapper.Map<UsuarioEntities>(usuarioRequest);
        var usuarioDatabase = await _usuarioRepository.GetById((int)id);
        usuarioDatabase.Email = usuarioRequest.Email;
        // usuarioDatabase = requestUsuarioEntities;
        var usuarioEditado = await _usuarioRepository.Put(usuarioDatabase, null);
        return _mapper.Map<UsuarioResponse>(usuarioEditado);
    }
    public async Task Delete(int id) 
    {
        var usuarioDatabase = await _usuarioRepository.GetById(id);
        if(usuarioDatabase != null)
        {
            await _usuarioRepository.Delete(usuarioDatabase);
        }
    }

    public async Task Delete(UsuarioRequest usuarioRequest)
    {
        throw new NotImplementedException();
    }
}