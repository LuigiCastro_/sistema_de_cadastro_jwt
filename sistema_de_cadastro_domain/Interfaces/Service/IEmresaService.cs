using sistema_de_cadastro_domain_contracts;

namespace sistema_de_cadastro_domain_interfaces;
public interface IEmpresaService : IBaseCRUD<EmpresaRequest, EmpresaResponse>
{
    Task<EmpresaResponse> GetByCnpj(string cnpj);
}