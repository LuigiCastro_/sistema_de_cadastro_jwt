using Microsoft.EntityFrameworkCore.Metadata.Builders;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository_mappings;

public class EndereçoEntitiesMap : IEntityTypeConfiguration<EndereçoEntities>
{
    public void Configure(EntityTypeBuilder<EndereçoEntities> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).HasColumnName("id");
        builder.Property(x => x.Ativo).HasColumnName("ativo");
        builder.Property(x => x.DataCriaçao).HasColumnName("dataCriaçao");
        builder.Property(x => x.DataAlteraçao).HasColumnName("dataAlteraçao");
        builder.Property(x => x.Rua).HasColumnName("rua");
        builder.Property(x => x.Bairro).HasColumnName("bairro");
        builder.Property(x => x.Cep).HasColumnName("cep");
        builder.Property(x => x.Cidade).HasColumnName("cidade");
        builder.Property(x => x.Estado).HasColumnName("estado");
        builder.Property(x => x.Numero).HasColumnName("numero");
        builder.ToTable("endereços");
    }
}
