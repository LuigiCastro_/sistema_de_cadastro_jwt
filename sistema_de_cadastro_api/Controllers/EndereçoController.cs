using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;

namespace sistema_de_cadastro_api_controller
{
    [ApiController]
    [Route("[controller]")]
    public class EndereçoController : ControllerBase
    {
        private readonly IEndereçoService _endereçoService;
        // private readonly IMapper _mapper;
        public EndereçoController(IEndereçoService endereçoService)
        {
            _endereçoService = endereçoService;
        }

        
        /// <summary>
        /// Por meio desta rota, é possível cadastrar uma endereço.
        /// </summary>
        [HttpPost]
        public async Task<EndereçoResponse> Post(EndereçoRequest endereçoRequest)
        {
            return await _endereçoService.Post(endereçoRequest);
        }


        /// <summary>
        /// Por meio desta rota, é possível buscar uma listagem de endereços cadastrados. 
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<EndereçoResponse>> Get()
        {
            return await _endereçoService.Get();
        }


        // /// <summary> 
        // /// Por meio desta rota, é possível buscar um endereço cadastrado, via ID.
        // /// </summary>
        // /// <param name="id"></param>
        // /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        // [HttpGet("{id}")]
        // public async Task<EndereçoResponse> GetById(int id)
        // {
        //     return await _endereçoService.GetById(id);
        // }


        /// <summary>
        /// Por meio desta rota, é possível buscar um endereço cadastrado, via CEP.
        /// </summary>
        /// <param name="cep"></param>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpGet("{cep}")]
        public async Task<EndereçoResponse> GetByCep(string cep, int numero)
        {
            return await _endereçoService.GetByCep(cep, numero);
        }


        /// <summary>
        /// Por meio desta rota, é possível editar um dado de um endereço cadastrado.
        /// </summary>
        [HttpPut("{id}")]
        public async Task<EndereçoResponse> Put(EndereçoRequest endereçoRequest, int id)
        {
            return await _endereçoService.Put(endereçoRequest, id);
        }


        /// <summary>
        /// Por meio desta rota, é possível deletar um endereço cadastrado.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _endereçoService.Delete(id);
        }
    }
}



