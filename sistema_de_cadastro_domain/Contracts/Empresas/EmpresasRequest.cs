using sistema_de_cadastro_domain_entities;

namespace sistema_de_cadastro_domain_contracts
{
    public class EmpresaRequest
    {
        public string Nome {get; set;}
        public string NomeFantasia {get; set;}
        public string Cnpj {get; set;}
        public EndereçoEntities Endereço {get; set;}
    }
}