using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_domain_enums;

namespace sistema_de_cadastro_domain_contracts
{
    public class UsuarioDto
    {
        public int Id {get; set;}
        public string Nome {get; set;}
        public string? Telefone {get; set;}
        public string Email {get; set;}
        public string DataNascimento {get; set;}
        public EndereçoEntities Endereço {get; set;}
        public string Permissao {get; set;}
    }
}