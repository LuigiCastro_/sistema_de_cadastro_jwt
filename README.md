# sistema_de_cadastro_jwt

# EXERCICIO - 07/11/2022

Com o uso de JWT no processo de Autentica��o, Identity no processo de valida��o de permiss�o e a documenta��o do swagger, utilizando ainda os princ�pios
de organiza��o de API trabalhados durante o curso deve ser implementada a seguinte solu��o:

- Eu, como Stella, vou me cadastrar na aplica��o e farei uso de e-mail e senha para autenticar
- Eu, como Stella, DEFINO minha permiss�o
- Eu, como Stella, vou buscar uma lista de todos os usu�rios cadastrados no sistema e posso visualizar o CPF deles, mas s� posso visualizar a listagem caso minha permiss�o for administrador
- Eu, como Stella, vou buscar uma lista de todos os usu�rios cadastrados no sistema e n�o posso visualizar o CPF deles, caso eu tenha a permiss�o de Cliente.

- Testes nas controllers e na camada de servi�o, testar tamb�m o m�todo de token e de cryptografia.
- Todos os campos com (*) s�o considerados obrigat�rios

As propriedades de um usu�rio �:

- *Nome
- Telefone
- *Email
- *Senha
- *Cpf
- *Data de Nascimento
- *Endere�o
	*Rua
	*Bairro
	*Cep
	*Cidade
	*Estado
	*Numero

CRUD de empresa

- Nome
- *Nome Fantasia
- *Endere�o
	*Rua
	*Bairro
	*Cep
	*Cidade
	*Estado
	*Numero

	--------------------------------------------------------------

	# COMEÇAR PELO GIT, CLONE, ADD, COMMIT, PUSH
	# CRIAR README
	# CRIAR GITIGNORE
	# CRIAR PASTAS E PROJETOS: SLN, WEBAPI, CLASSLIBS, TESTE
	# CRIAR AS PASTAS E ARQUIVOS INTERNOS
	# FAZER AS REFERENCIAS
	# PRODUZIR OS ARQUIVOS DE CODIGOS:
	_Começar por Domain > Entities, Contracts


	> Quantas e quais entidades criar? 3 : USUARIO, EMPRESA, E ENDEREÇOS
	> CRUD



	--------------------------------------------------------------


# IDENTITY
não se trabalha com identity em aplicações publicas... facilidade de descriptografar.
trabalha-se apenas em aplicações bem particulares.

> BEARER
> MD5 HASH ---- representa uma string
> JWT --------- representa um objeto

**Hash é mais fácil de quebrar. Por isso, para autenticação se trabalha com Token.