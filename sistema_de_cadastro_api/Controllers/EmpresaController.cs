using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;


namespace sistema_de_cadastro_api_controller
{
    [ApiController]
    [Route("[controller]")]
    public class EmpresaController : ControllerBase
    {
        private readonly IEmpresaService _empresaService;
        // private readonly IMapper _mapper;

        public EmpresaController(IEmpresaService empresaService)
        {
            _empresaService = empresaService;
        }
        
        /// <summary>
        /// Por meio desta rota, é possível cadastrar uma empresa.
        /// </summary>
        [HttpPost]
        public async Task<EmpresaResponse> Post(EmpresaRequest empresaRequest)
        {
            return await _empresaService.Post(empresaRequest);
        }

        /// <summary>
        /// Por meio desta rota, é possível buscar uma listagem de empresas cadastradas.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<EmpresaResponse>> Get()
        {
            return await _empresaService.Get();
        }
        
        // /// <summary> 
        // /// Por meio desta rota, é possível buscar uma empresa cadastrada, via ID.
        // /// </summary>
        // /// <param name="id"></param>
        // /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        // [HttpGet("{id}")]
        // // public async Task<IActionResult<EmpresaResponse>> GetById(int id)
        // public async Task<EmpresaResponse> GetById(int id)
        // {
        //     return await _empresaService.GetById(id);
        //     //ActionResult = resultado de uma requisição
        // }
        
        /// <summary>
        /// Por meio desta rota, é possível buscar uma empresa cadastrada, via CNPJ.
        /// </summary>
        /// <param name="cnpj"></param>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpGet("{cnpj}")]
        public async Task<EmpresaResponse> GetByCnpj(string cnpj)
        {
            return await _empresaService.GetByCnpj(cnpj);
        }


        /// <summary>
        /// Por meio desta rota, é possível editar um dado de uma empresa cadastrada.
        /// </summary>
        [HttpPut("{id}")]
        public async Task<EmpresaResponse> Put(EmpresaRequest empresaRequest, int id)
        {
            return await _empresaService.Put(empresaRequest, id);
        }

        /// <summary>
        /// Por meio desta rota, é possível deletar uma empresa cadastrada.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _empresaService.Delete(id);
        }

    }
}



