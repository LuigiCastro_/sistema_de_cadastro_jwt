using sistema_de_cadastro_teste_crosscutting;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_api_controller;
using sistema_de_cadastro_teste_faker;
using sistema_de_cadastro_service;
using AutoMapper;
using Xunit;
using Bogus;
using Moq;

namespace sistema_de_cadastro_teste_controller
{
    [Trait("Controller", "Controller de Usuarios")]
    public class UsuarioControllerTeste
    {
        private readonly Mock<IUsuarioService> _mockUsuarioService = new Mock<IUsuarioService>();


        [Fact(DisplayName = "Cadastra um novo usuario")]
        public async Task Post()
        {
            var userRequest = UsuarioContractsFaker.UsuarioCadastroRequestObjectFake();
            var resultUserRequest = UsuarioContractsFaker.UsuarioResponseNameFakeAsync(userRequest.Nome);

            _mockUsuarioService.Setup(mock => mock.Post(userRequest)).Returns(resultUserRequest);

            var controller = new UsuarioController(_mockUsuarioService.Object);
            var result = await controller.Post(userRequest);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);

        }
        
        
        [Fact(DisplayName = "Obtém uma lista de usuários")]
        public async Task Get()
        {
            _mockUsuarioService.Setup(mock => mock.Get()).Returns(UsuarioContractsFaker.UsuarioResponseListFakeAsync());

            var controller = new UsuarioController(_mockUsuarioService.Object);
            var result = await controller.Get();

            Assert.True(result.ToList().Count() > 0);
        }


        [Fact(DisplayName = "Obtém um usuário, via ID")]
        public async Task GetByCpf()
        {
            string cpf = UsuarioContractsFaker.GetCPFFake();

            _mockUsuarioService.Setup(mock => mock.GetByCpf(cpf)).Returns(UsuarioContractsFaker.UsuarioResponseCPFFakeAsync(cpf));

            var controller = new UsuarioController(_mockUsuarioService.Object);
            var result = await controller.GetByCpf(cpf);

            Assert.Equal(result.CPF, cpf);
        }


        [Fact(DisplayName = "Edita um dado de um usuário")]
        public async Task Put()
        {
            var userRequest = UsuarioContractsFaker.UsuarioRequestNameFake();
            var resultUserRequest = UsuarioContractsFaker.UsuarioResponseNameFakeAsync(userRequest.Nome);

            _mockUsuarioService.Setup(mock => mock.Put(userRequest, resultUserRequest.Result.Id)).Returns(resultUserRequest);

            var controller = new UsuarioController(_mockUsuarioService.Object);
            var result = await controller.Put(userRequest, resultUserRequest.Result.Id);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove um usuário")]
        public async Task Delete()
        {
            int id = UsuarioContractsFaker.GetIdFake();

            _mockUsuarioService.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var controller = new UsuarioController(_mockUsuarioService.Object);

            try
            {
                await controller.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }
        }
    }
}
