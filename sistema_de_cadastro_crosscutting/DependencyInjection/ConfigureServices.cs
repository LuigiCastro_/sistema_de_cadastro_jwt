using Microsoft.Extensions.DependencyInjection;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_service;

namespace sistema_de_cadastro_crosscutting;
public class ConfigureServices
{
    // INJEÇAO DE DEPENDECIAS <INTERFACE, SERVICE>
    public static void ConfigureDependenciesServices(IServiceCollection serviceCollection)
    {
        serviceCollection.AddScoped<IEmpresaService, EmpresaService>();
        serviceCollection.AddScoped<IEndereçoService, EndereçoService>();
        serviceCollection.AddScoped<IUsuarioService, UsuarioService>();

        // Autenticação
        serviceCollection.AddScoped<IAuthenticationService, AuthenticationService>();
    }

}
