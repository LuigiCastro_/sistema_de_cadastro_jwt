using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_domain_contracts;
using AutoMapper;

namespace sistema_de_cadastro_crosscutting_mappers;
public class EmpresaEntitiesToContractMap : Profile
{

    // CRIANDO AUTOMAPPER ENTRE ENTITY E REQUEST/RESPONSE
    public EmpresaEntitiesToContractMap()
    {
        CreateMap<EmpresaEntities, EmpresaRequest>().ReverseMap();
        CreateMap<EmpresaEntities, EmpresaResponse>().ReverseMap();
    }    
}