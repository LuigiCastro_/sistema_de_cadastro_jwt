using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;


namespace sistema_de_cadastro_api_controller
{
    [ApiController]
    [Route("[controller]")]
    // [Authorize(Roles = "Administrador")]
    public class UsuarioController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;
        // private readonly IMapper _mapper;
        public UsuarioController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        
        /// <summary>
        /// Por meio desta rota, é possível cadastrar um usuário.
        /// </summary>
        [HttpPost]
        [AllowAnonymous]
        public async Task<UsuarioResponse> Post(UsuarioCadastroRequest usuarioRequest) // Neste caso, utiliza-se o UsuarioCadastroRequest para se ter acesso à senha.
        {
            return await _usuarioService.PostCadastroRequest(usuarioRequest);
        }
                
        
        // /// <summary>
        // /// Por meio desta rota, é possível buscar uma listagem de usuarios cadastrados sem CPF. 
        // /// </summary>
        // [HttpGet]
        // [Authorize(Roles = "Cliente")]
        // public async Task<IEnumerable<UsuarioDto>> GetWithoutCpf()
        // {
        //     return await _usuarioService.GetWithoutCpf();
        // }


        /// <summary>
        /// Por meio desta rota, é possível buscar uma listagem de usuarios cadastrados com CPF. 
        /// </summary>
        [HttpGet]
        [Authorize(Roles = "Administrador")]
        public async Task<IEnumerable<UsuarioResponse>> Get()
        {
            return await _usuarioService.Get();
        }


        /// <summary>
        /// Por meio desta rota, é possível buscar um usuário cadastrado, via CPF.
        /// </summary>
        /// <param name="cpf"></param>
        /// <response code="200">Sucesso, e retorna o elemento encontrado via ID</response>
        [HttpGet("{cpf}")]
        public async Task<UsuarioResponse> GetByCpf(string cpf)
        {
            return await _usuarioService.GetByCpf(cpf);
        }


        /// <summary>
        /// Por meio desta rota, é possível editar um dado de um usuário cadastrado.
        /// </summary>
        [HttpPut("{id}")]
        public async Task<UsuarioResponse> Put(UsuarioRequest usuarioRequest, int id)
        {
            return await _usuarioService.Put(usuarioRequest, id);
        }


        /// <summary>
        /// Por meio desta rota, é possível deletar um usuário cadastrado.
        /// </summary>
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _usuarioService.Delete(id);
        }

    }
}



