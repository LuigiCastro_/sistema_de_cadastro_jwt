using Microsoft.Extensions.DependencyInjection;
using sistema_de_cadastro_repository_context;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_repository;
using Microsoft.EntityFrameworkCore;


namespace sistema_de_cadastro_crosscutting;

public static class ConfiureRepository
{
    
    // INJEÇAO DE DEPENDECIAS <INTERFACE, REPOSITORY>
    public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection, string connectionString)
    {
        serviceCollection.AddScoped<IEmpresaRepository, EmpresaRepository>();
        serviceCollection.AddScoped<IEndereçoRepository, EndereçoRepository>();
        serviceCollection.AddScoped<IUsuarioRepository, UsuarioRepository>();
        
        serviceCollection.AddDbContext<Context>(options => options.UseSqlServer(connectionString));
    }
}