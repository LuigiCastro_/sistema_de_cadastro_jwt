namespace sistema_de_cadastro_domain_entities
{
    public class BaseEntities
    {
        public BaseEntities()
        {
            this.Ativo = true;
        }
        
        public int Id {get; set;}
        public bool Ativo {get; set;}
        public DateTime DataCriaçao {get; set;}
        public DateTime? DataAlteraçao {get; set;}
    }
}