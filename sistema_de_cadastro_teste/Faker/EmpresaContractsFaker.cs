using sistema_de_cadastro_domain_contracts;
using Bogus.Extensions.Brazil;
using Bogus;

namespace sistema_de_cadastro_teste_faker;

public class EmpresaContractsFaker
{
    private static readonly Faker Fake = new Faker();


    public static int GetIdFake()
    {
        return Fake.IndexFaker;
    }
    public static string GetCNPJFake()
    {
        return Fake.Company.Cnpj();
    }

    public static async Task<IEnumerable<EmpresaResponse>> EmpresaResponseListFakeAsync()
    {
        var minhaLista = new List<EmpresaResponse>();

        for (int i = 0; i < 5; i++)
        {
            minhaLista.Add(new EmpresaResponse()
            {
                Id = i,
                Nome = Fake.Name.FirstName(),
                NomeFantasia = Fake.Name.JobTitle(),
                Cnpj = Fake.Company.Cnpj(),
                // Endereço
            });
        }

        return minhaLista;
    }


    public static EmpresaRequest EmpresaRequestNameFake()
    {
        return new EmpresaRequest
        {
            Nome = Fake.Name.FirstName()
        };
    }

    public static async Task<EmpresaResponse> EmpresaResponseIdFakeAsync(int id)
    {
        return new EmpresaResponse()
        {
            Id = id,
            Nome = Fake.Name.FirstName()
        };
    }
    public static async Task<EmpresaResponse> EmpresaResponseNameFakeAsync(string nome)
    {
        return new EmpresaResponse()
        {
            Id = Fake.IndexFaker,
            Nome = nome
        };
    }
    
    public static async Task<EmpresaResponse> EmpresaResponseCNPJFakeAsync(string cnpj)
    {
        return new EmpresaResponse()
        {
            Id = Fake.IndexFaker,
            Cnpj = cnpj
        };
    }
}