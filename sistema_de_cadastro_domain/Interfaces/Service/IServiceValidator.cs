using sistema_de_cadastro_domain_contracts;

namespace sistema_de_cadastro_domain_interfaces;
public interface IServiceValidator
{
    void ValidarCadastroUsuario(UsuarioCadastroRequest usuarioRequest);
}