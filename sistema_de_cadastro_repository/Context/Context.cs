using sistema_de_cadastro_repository_mappings;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository_context;


// CRIAÇÃO DE TABELAS PARA O BANCO DE DADOS
public class Context : DbContext
{

    // CRIACAO DE UM CONSTRUTOR VAZIO
    public Context() {}

    // CRIÇAO DAS TABELAS
    public DbSet<EmpresaEntities> Empresas {get; set;}
    public DbSet<EndereçoEntities> Endereços {get; set;}
    public DbSet<UsuarioEntities> Usuarios {get; set;}


    // ??
    public Context(DbContextOptions<Context> options) : base(options) {}

    
    // CONEXAO COM O BANCO DE DADOS (RETIRAR ESSA CONEXAO?)
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
       optionsBuilder.UseSqlServer(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
    }


    
    // CONFIGURAÇAO ENTRE ENTITIES E MAP
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<EmpresaEntities>(new EmpresaEntitiesMap().Configure);
        modelBuilder.Entity<EndereçoEntities>(new EndereçoEntitiesMap().Configure);
        modelBuilder.Entity<UsuarioEntities>(new UsuarioEntitiesMap().Configure);

        modelBuilder.Entity<UsuarioEntities>().HasOne(s => s.Endereço);
        modelBuilder.Entity<EmpresaEntities>().HasOne(s => s.Endereço);
    }
}