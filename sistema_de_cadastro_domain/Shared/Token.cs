//FW18N7OyCGZNlbI/j73gIMXpXDhbJPVHNq72/pSw
using sistema_de_cadastro_domain_entities;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;


namespace sistema_de_cadastro_domain_shared;

public class Token
{
    public static string GenerateToken(UsuarioEntities user)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        var key = Encoding.ASCII.GetBytes("FW18N7OyCGZNlbI/j73gIMXpXDhbJPVHNq72/pSw");
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.GivenName, user.CPF.ToString()),
                new Claim(ClaimTypes.SerialNumber, user.Id.ToString()),
                // new Claim(ClaimTypes.Role, "Administrador".ToString()),
                new Claim(ClaimTypes.Role, user.Permissao.ToString())
            }),

            Expires = DateTime.Now.AddHours(8),
            SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        };
        var token = tokenHandler.CreateToken(tokenDescriptor);
        return tokenHandler.WriteToken(token);
    }
}