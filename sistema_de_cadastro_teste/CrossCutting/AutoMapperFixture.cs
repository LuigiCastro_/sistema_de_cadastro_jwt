using sistema_de_cadastro_crosscutting_mappers;
using AutoMapper;
using System;

namespace sistema_de_cadastro_teste_crosscutting
{
    public abstract class BaseAutoMapperFixture
    {
        public IMapper mapper { get; set; }
        public BaseAutoMapperFixture()
        {
            mapper = new AutoMapperFixture().GetMapper();
        }
    }

    public class AutoMapperFixture : IDisposable
    {
        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UsuarioEntitiesToContractMap());
                cfg.AddProfile(new EmpresaEntitiesToContractMap());
                cfg.AddProfile(new EndereçoEntitiesToContractMap());
            });

            return config.CreateMapper();
        }

        public void Dispose() { }
    }
}
