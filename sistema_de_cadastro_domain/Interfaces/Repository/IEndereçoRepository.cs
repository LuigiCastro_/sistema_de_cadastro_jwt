using sistema_de_cadastro_domain_entities;

namespace sistema_de_cadastro_domain_interfaces;
public interface IEndereçoRepository : IBaseCRUD<EndereçoEntities, EndereçoEntities>
{
    Task<EndereçoEntities> GetByCep(string cep, int numero);
    Task Delete(EndereçoEntities endereçoEntities);

}