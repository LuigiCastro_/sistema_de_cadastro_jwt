using sistema_de_cadastro_repository_context;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository;

public class UsuarioRepository : IUsuarioRepository
{
    private readonly Context _context;      //  DbContext é quem permite o acesso aos métodos: ADD, FIND, EDIT, REMOVE, SAVE...
    public UsuarioRepository(Context context)
    {
        _context = context;
    }
    public async Task<UsuarioEntities> Post(UsuarioEntities usuarioEntities) 
    {
        await _context.Usuarios.AddAsync(usuarioEntities);
        await _context.SaveChangesAsync();
        return usuarioEntities;
    }
    public async Task<IEnumerable<UsuarioEntities>> Get() 
    {
        return await _context.Usuarios.Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .ToListAsync();
    }

    public async Task<IEnumerable<UsuarioEntities>> GetWithoutCpf()
    {
        return await _context.Usuarios.Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .ToListAsync();
    }

    public async Task<UsuarioEntities> GetById(int id) 
    {
        return await _context.Usuarios.Where(prop => prop.Id == id)
                                    .Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
    }
    public async Task<UsuarioEntities> GetByCpf(string cpf)
    {
        return await _context.Usuarios.Where(prop => prop.CPF == cpf)
                                    .Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
    }
    public async Task<UsuarioEntities> GetByEmail(string email)
    {
        return await _context.Usuarios.Where(prop => prop.Email == email)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
    }

    public async Task<UsuarioEntities> Put(UsuarioEntities usuarioEntities, int? id) 
    {
        _context.Usuarios.Update(usuarioEntities);  
        // _context.Entry(usuarioEntities).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return usuarioEntities;
    }
    public async Task Delete(int id) 
    {
        throw new NotImplementedException();
    }

    public async Task Delete(UsuarioEntities usuarioEntities)
    {
        _context.Usuarios.Remove(usuarioEntities);
        await _context.SaveChangesAsync();
    }
}