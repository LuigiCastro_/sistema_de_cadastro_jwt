using sistema_de_cadastro_teste_crosscutting;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_teste_faker;
using sistema_de_cadastro_repository;
using sistema_de_cadastro_service;
using AutoMapper;
using Xunit;
using Bogus;
using Moq;

namespace sistema_de_cadastro_teste_service;

public class EmpresaServiceTeste
{
    private readonly Mock<IEmpresaRepository> _mockEmpresaRepository = new Mock<IEmpresaRepository>();
    private IMapper mapper = new AutoMapperFixture().GetMapper();


    [Fact(DisplayName = "Cadastra uma nova empresa")]
    public async Task Post()
    {
        var empresaRequest = EmpresaContractsFaker.EmpresaRequestNameFake();
        var empresaRequestEntities = EmpresaEntitiesFaker.EmpresaEntitiesNameFake();
        var resultEmpresaRequest = EmpresaEntitiesFaker.EmpresaEntitiesNameFakeAsync(empresaRequestEntities.Nome);

        _mockEmpresaRepository.Setup(mock => mock.Post(It.IsAny<EmpresaEntities>())).Returns(resultEmpresaRequest);
        
        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);
        var result = await service.Post(empresaRequest);

        Assert.Equal(result.Nome, resultEmpresaRequest.Result.Nome);
    }


    [Fact(DisplayName = "Obtém uma lista de empresas")]
    public async Task Get()
    {
        _mockEmpresaRepository.Setup(mock => mock.Get()).Returns(EmpresaEntitiesFaker.EmpresaEntitiesListFakeAsync);
        
        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);
        var result = await service.Get();

        Assert.True(result.ToList().Count() > 0);
    }


    [Fact(DisplayName = "Obtém uma empresa, via ID")]
    public async Task GetById()
    {
        int id = EmpresaEntitiesFaker.GetIdFake();
        _mockEmpresaRepository.Setup(mock => mock.GetById(id)).Returns(EmpresaEntitiesFaker.EmpresaEntitiesIdFakeAsync(id));

        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);
        var result = await service.GetById(id);

        Assert.Equal(result.Id, id);
    }


    [Fact(DisplayName = "Edita um dado de uma empresa")]
    public async Task PutName()
    {
        var empresaRequest = EmpresaContractsFaker.EmpresaRequestNameFake();
        var empresaRequestEntities = EmpresaEntitiesFaker.EmpresaEntitiesNameFake();
        var resultEmpresaRequest = EmpresaEntitiesFaker.EmpresaEntitiesNameFakeAsync(empresaRequestEntities.Nome);

        _mockEmpresaRepository.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(resultEmpresaRequest);
        _mockEmpresaRepository.Setup(mock => mock.Put(It.IsAny<EmpresaEntities>(), It.IsAny<int?>())).Returns(resultEmpresaRequest);

        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);
        var result = await service.Put(empresaRequest, resultEmpresaRequest.Result.Id);

        Assert.Equal(result.Nome, resultEmpresaRequest.Result.Nome);
    }


    [Fact(DisplayName = "Remove uma empresa")]
    public async Task Delete()
    {
        int id = EmpresaEntitiesFaker.GetIdFake();

        _mockEmpresaRepository.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);

        try
        {
            await service.Delete(id);
        }
        catch (System.Exception)
        {
            Assert.True(false);
        }
    }


    [Fact(DisplayName = "Obtém uma empresa, via CNPJ")]
    public async Task GetByCnpj()
    {
        string cnpj = EmpresaEntitiesFaker.GetCNPJFake();
        _mockEmpresaRepository.Setup(mock => mock.GetByCnpj(cnpj)).Returns(EmpresaEntitiesFaker.EmpresaEntitiesCNPJFakeAsync(cnpj));

        var service = new EmpresaService(_mockEmpresaRepository.Object, mapper);
        var result = await service.GetByCnpj(cnpj);

        Assert.Equal(result.Cnpj, cnpj);
    }
}
