using sistema_de_cadastro_teste_crosscutting;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_api_controller;
using sistema_de_cadastro_teste_faker;
using sistema_de_cadastro_service;
using AutoMapper;
using Xunit;
using Bogus;
using Moq;

namespace sistema_de_cadastro_teste_controller
{
    [Trait("Controller", "Controller de Empresas")]
    public class EmpresaControllerTeste
    {
        private readonly Mock<IEmpresaService> _mockEmpresaService = new Mock<IEmpresaService>();


        [Fact(DisplayName = "Cadastra uma nova empresa")]
        public async Task Post()
        {
            var userRequest = EmpresaContractsFaker.EmpresaRequestNameFake();
            var resultUserRequest = EmpresaContractsFaker.EmpresaResponseNameFakeAsync(userRequest.Nome);

            _mockEmpresaService.Setup(mock => mock.Post(userRequest)).Returns(resultUserRequest);

            var controller = new EmpresaController(_mockEmpresaService.Object);
            var result = await controller.Post(userRequest);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }
        
        
        [Fact(DisplayName = "Obtém uma lista de empresas")]
        public async Task Get()
        {
            _mockEmpresaService.Setup(mock => mock.Get()).Returns(EmpresaContractsFaker.EmpresaResponseListFakeAsync());

            var controller = new EmpresaController(_mockEmpresaService.Object);
            var result = await controller.Get();

            Assert.True(result.ToList().Count() > 0);
        }


        [Fact(DisplayName = "Obtém uma empresa, via CNPJ")]
        public async Task GetByCnpj()
        {
            string cnpj = EmpresaContractsFaker.GetCNPJFake();

            _mockEmpresaService.Setup(mock => mock.GetByCnpj(cnpj)).Returns(EmpresaContractsFaker.EmpresaResponseCNPJFakeAsync(cnpj));

            var controller = new EmpresaController(_mockEmpresaService.Object);
            var result = await controller.GetByCnpj(cnpj);

            Assert.Equal(result.Cnpj, cnpj);
        }


        [Fact(DisplayName = "Edita um dado de uma empresa")]
        public async Task Put()
        {
            var userRequest = EmpresaContractsFaker.EmpresaRequestNameFake();
            var resultUserRequest = EmpresaContractsFaker.EmpresaResponseNameFakeAsync(userRequest.Nome);

            _mockEmpresaService.Setup(mock => mock.Put(userRequest, resultUserRequest.Result.Id)).Returns(resultUserRequest);

            var controller = new EmpresaController(_mockEmpresaService.Object);
            var result = await controller.Put(userRequest, resultUserRequest.Result.Id);

            Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
        }

        [Fact(DisplayName = "Remove uma empresa")]
        public async Task Delete()
        {
            int id = EmpresaContractsFaker.GetIdFake();

            _mockEmpresaService.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

            var controller = new EmpresaController(_mockEmpresaService.Object);

            try
            {
                await controller.Delete(id);
            }
            catch (System.Exception)
            {
                Assert.True(false);
            }
        }
    }
}
