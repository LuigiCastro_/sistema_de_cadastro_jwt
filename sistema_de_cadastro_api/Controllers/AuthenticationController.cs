using sistema_de_cadastro_domain_interfaces;
using Microsoft.AspNetCore.Mvc;

namespace sistema_de_cadastro_api_controller;

[ApiController]
[Route("[controller]")]
public class AuthenticationController : ControllerBase
{
    private readonly IAuthenticationService _authenticationService;
    public AuthenticationController(IAuthenticationService authenticationService)
    {
        _authenticationService = authenticationService;
    }
    
    /// <summary>
    /// Por meio desta rota, é possível o usuário acessar sua conta, via Email e Senha.
    /// </summary>
    [HttpPost]
    public async Task<string> Login(string email, string password)
    {
        return await _authenticationService.Login(email, password);
    }
    
}


