namespace sistema_de_cadastro_domain_interfaces;

public interface IAuthenticationService
{
    Task<string> Login(string email, string password);
}