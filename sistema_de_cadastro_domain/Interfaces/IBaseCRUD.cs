namespace sistema_de_cadastro_domain_interfaces;

// COM O PARAMETRO REQUEST, RETORNA O RESPONSE
// NA CONTROLLER SÃO CHAMADOS O REQUEST E RESPONSE      [Controller acessa os metodos atravaes do _service]
// NO SERVICE SÃO CHAMADOS O REQUEST E RESPONSE         [Service acessa os metodos atravaes do _repository]
// NO REPOSITORY, CHAMA-SE APENAS O ENTITY              [Repository acessa os metodos atravaes do _context]
// REQUEST E RESPONSE AQUI SÃO TIPOS GENERICOS          [Context acessa os metodos atravaes do DbContext / EF library]
public interface IBaseCRUD<Request, Response>
{
    Task<Response> Post(Request request);
    Task<IEnumerable<Response>> Get();
    Task<Response> GetById(int id);
    Task<Response> Put(Request request, int? id);       // int? significa que é NULLABLE.
    Task Delete(int id);
}


// NULLABLE = true -------- valor anulável (não nulo). logo, opcional / não obrigatório.
// NULLABLE = false ------- valor não anulável. logo, obrigatório!