using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_shared;

namespace sistema_de_cadastro_service;

public class AuthenticationService : IAuthenticationService
{
    private readonly IUsuarioRepository _usuarioRepository;
    public AuthenticationService(IUsuarioRepository usuarioRepository)
    {
        _usuarioRepository = usuarioRepository;
    }

    public async Task<string> Login(string email, string password)
    {
        var result = await _usuarioRepository.GetByEmail(email);
        
        var encryptedPassword = Cryptography.Encrypt(password);
        if(result != null && result.Senha == encryptedPassword)
        {
            return Token.GenerateToken(result);
        }

        // var descrypetdPassword = Cryptography.Decrypt(result.Senha);
        // if(result != null && password == descrypetdPassword)
        // {
        //     return "authorized";
        // }
        
        throw new Exception("Senha inválida");
    }
}