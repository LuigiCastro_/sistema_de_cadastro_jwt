using sistema_de_cadastro_domain_contracts;

namespace sistema_de_cadastro_domain_interfaces;
public interface IEndereçoService : IBaseCRUD<EndereçoRequest, EndereçoResponse>
{
    Task<EndereçoResponse> GetByCep(string cep, int numero);
}