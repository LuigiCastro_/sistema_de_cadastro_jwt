using sistema_de_cadastro_domain_entities;
using Bogus.Extensions.Brazil;
using Bogus;

namespace sistema_de_cadastro_teste_faker;

public class UsuarioEntitiesFaker
{
    private static readonly Faker Fake = new Faker();

    public static int GetIdFake()
    {
        return Fake.IndexFaker;
    }
    public static string GetCPFFake()
    {
        return Fake.Person.Cpf();
    }

    public static async Task<IEnumerable<UsuarioEntities>> UsuarioEntitiesListFakeAsync()
    {
        var minhaLista = new List<UsuarioEntities>();

        for (int i = 0; i < 5; i++)
        {
            minhaLista.Add(new UsuarioEntities()
            {
                Id = i,
                Nome = Fake.Name.FirstName(),
                Telefone = Fake.Person.Phone,
                Email = Fake.Internet.Email(),
                CPF = Fake.Person.Cpf(),
                // // DataNascimento = Fake.Person.DateOfBirth,
                // Endereço
            });
        }

        return minhaLista;
    }


    public static UsuarioEntities UsuarioEntitiesNameFake()
    {
        return new UsuarioEntities
        {
            Nome = Fake.Name.FirstName()
        };
    }
    public static UsuarioEntities UsuarioEntitiesEmailFake()
    {
        return new UsuarioEntities
        {
            Email = Fake.Internet.Email()
        };
    }

    public static async Task<UsuarioEntities> UsuarioEntitiesIdFakeAsync(int id)
    {
        return new UsuarioEntities()
        {
            Id = id,
            Nome = Fake.Name.FirstName()
        };
    }
    public static async Task<UsuarioEntities> UsuarioEntitiesNameFakeAsync(string nome)
    {
        return new UsuarioEntities()
        {
            Id = Fake.IndexFaker,
            Nome = nome
        };
    }
    public static async Task<UsuarioEntities> UsuarioEntitiesEmailFakeAsync(string email)
    {
        return new UsuarioEntities()
        {
            Id = Fake.IndexFaker,
            Email = email
        };
    }
    public static async Task<UsuarioEntities> UsuarioEntitiesCPFFakeAsync(string cpf)
    {
        return new UsuarioEntities()
        {
            Id = Fake.IndexFaker,
            CPF = cpf
        };
    }
}