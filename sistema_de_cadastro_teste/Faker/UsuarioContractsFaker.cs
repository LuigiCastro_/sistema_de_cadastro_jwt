using sistema_de_cadastro_domain_contracts;
using Bogus.Extensions.Brazil;
using Bogus;

namespace sistema_de_cadastro_teste_faker;

public class UsuarioContractsFaker
{
    private static readonly Faker Fake = new Faker();

    public static int GetIdFake()
    {
        return Fake.IndexFaker;
    }

    public static string GetCPFFake()
    {
        return Fake.Person.Cpf();
    }


    public static async Task<IEnumerable<UsuarioResponse>> UsuarioResponseListFakeAsync()
    {
        var minhaLista = new List<UsuarioResponse>();

        for (int i = 0; i < 5; i++)
        {
            minhaLista.Add(new UsuarioResponse()
            {
                Id = i,
                Nome = Fake.Name.FirstName(),
                Telefone = Fake.Person.Phone,
                Email = Fake.Internet.Email(),
                CPF = Fake.Person.Cpf(),
            });
        }

        return minhaLista;
    }

    public static async Task<UsuarioResponse> UsuarioResponseIdFakeAsync(int id)
    {
        return new UsuarioResponse()
        {
            Id = id,
            Nome = Fake.Name.FirstName()
        };
    }

    public static UsuarioRequest UsuarioRequestNameFake()
    {
        return new UsuarioRequest
        {
            Nome = Fake.Name.FirstName()
        };
    }
    public static UsuarioRequest UsuarioRequestEmailFake()
    {
        return new UsuarioRequest
        {
            Email = Fake.Internet.Email()
        };
    }

    public static UsuarioResponse UsuarioResponseNameFake()
    {
        return new UsuarioResponse
        {
            Nome = Fake.Name.FirstName()
        };
    }
    public static UsuarioCadastroRequest UsuarioCadastroRequestNameFake()
    {
        return new UsuarioCadastroRequest
        {
            Nome = Fake.Name.FirstName()
        };
    }

    public static UsuarioCadastroRequest UsuarioCadastroRequestNamePasswordFake()
    {
        return new UsuarioCadastroRequest
        {
            Nome = Fake.Name.FirstName(),
            Senha = Fake.Name.FullName()
        };
    }

    public static async Task<UsuarioResponse> UsuarioResponseNameFakeAsync(string nome)
    {
        return new UsuarioResponse()
        {
            Id = Fake.IndexFaker,
            Nome = nome,
            Telefone = Fake.Person.Phone,
            Email = Fake.Internet.Email(),
            CPF = Fake.Person.Cpf(),
            DataNascimento = "10/10/2000",
            Permissao = "Administrador",
        };
    }
    public static async Task<UsuarioResponse> UsuarioResponseCPFFakeAsync(string cpf)
    {
        return new UsuarioResponse()
        {
            Id = Fake.IndexFaker,
            CPF = cpf
        };
    }

    public static UsuarioCadastroRequest UsuarioCadastroRequestObjectFake()
    {
        return new UsuarioCadastroRequest
        {
            Nome = Fake.Name.FirstName(),
            Telefone = Fake.Person.Phone,
            Email = Fake.Internet.Email(),
            Senha = "Admin@123",
            CPF = Fake.Person.Cpf(),
            DataNascimento = "10/10/2000",
            Permissao = "Administrador",
        };
    }
    public static UsuarioRequest UsuarioRequestObjectFake()
    {
        return new UsuarioRequest
        {
            Nome = Fake.Name.FirstName(),
            Telefone = Fake.Person.Phone,
            Email = Fake.Internet.Email(),
            CPF = Fake.Person.Cpf(),
            DataNascimento = "10/10/2000",
            Permissao = "Administrador",
        };
    }

}


