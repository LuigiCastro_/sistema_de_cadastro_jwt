namespace sistema_de_cadastro_domain_contracts
{
    public class EndereçoRequest
    {
        public string Rua {get; set;}
        public string Bairro {get; set;}
        public string Cep {get; set;}
        public string Cidade {get; set;}
        public string Estado {get; set;}
        public int Numero {get; set;}
    }
}