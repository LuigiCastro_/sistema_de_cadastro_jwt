﻿using Microsoft.Extensions.DependencyInjection;
using sistema_de_cadastro_crosscutting;
namespace sistema_de_cadastro_ioc;

// APLICACAO DAS CLASSES E METODOS DA DEPENDENCY INJECTION
public static class NativeInjectorBootStrapper
{
    public static void RegisterAppDependencies(this IServiceCollection services)
    {
        ConfigureServices.ConfigureDependenciesServices(services);
        ConfigureMappers.ConfigureDependenciesMappers(services);
    }
    public static void RegisterAppDependenciesContext(this IServiceCollection services, string connectionString)
    {
        ConfiureRepository.ConfigureDependenciesRepository(services, connectionString);

        // Conection string 
    }
}
