using sistema_de_cadastro_repository_context;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository;

public class EndereçoRepository : IEndereçoRepository
{
    private readonly Context _context;      //  DbContext é quem permite o acesso aos métodos: ADD, FIND, EDIT, REMOVE, SAVE...
    public EndereçoRepository(Context context)
    {
        _context = context;
    }
    public async Task<EndereçoEntities> Post(EndereçoEntities endereçoEntities) 
    {
        await _context.Endereços.AddAsync(endereçoEntities);
        await _context.SaveChangesAsync();
        return endereçoEntities;
    }
    public async Task<IEnumerable<EndereçoEntities>> Get() 
    {
        return await _context.Endereços.AsNoTracking().ToListAsync();
    }
    public async Task<EndereçoEntities> GetById(int id) 
    {
        return await _context.Endereços.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
    }
    public async Task<EndereçoEntities> GetByCep(string cep, int numero)
    {
        return await _context.Endereços.Where(prop => prop.Cep == cep && prop.Numero == numero).AsNoTracking().FirstOrDefaultAsync();
    }
    public async Task<EndereçoEntities> Put(EndereçoEntities endereçoEntities, int? id) 
    {
        _context.Endereços.Update(endereçoEntities);
        await _context.SaveChangesAsync();
        return endereçoEntities;
    }
    public async Task Delete(int id) 
    {
        throw new NotImplementedException();
    }

    public async Task Delete(EndereçoEntities endereçoEntities)
    {
        _context.Endereços.Remove(endereçoEntities);
        await _context.SaveChangesAsync();
    }
}