using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using AutoMapper;

namespace sistema_de_cadastro_service;
public class EndereçoService : IEndereçoService
{
    private readonly IEndereçoRepository _endereçoRepository;
    private readonly IMapper _mapper;
    public EndereçoService(IEndereçoRepository endereçoRepository, IMapper mapper)
    {
        _endereçoRepository = endereçoRepository;
        _mapper = mapper;
    }
    public async Task<EndereçoResponse> Post(EndereçoRequest endereçoRequest) 
    {
        var requestEndereçoEntities = _mapper.Map<EndereçoEntities>(endereçoRequest);
        var endereçoCadastrado = await _endereçoRepository.Post(requestEndereçoEntities);
        return _mapper.Map<EndereçoResponse>(endereçoCadastrado);
    }
    public async Task<IEnumerable<EndereçoResponse>> Get() 
    {
        var listaEndereçosDatabase = await _endereçoRepository.Get();
        return _mapper.Map<IEnumerable<EndereçoResponse>>(listaEndereçosDatabase);
    }
    public async Task<EndereçoResponse> GetById(int id) 
    {
        var endereçoEncontrado = await _endereçoRepository.GetById(id);
        return _mapper.Map<EndereçoResponse>(endereçoEncontrado);
    }
    public async Task<EndereçoResponse> GetByCep(string cep, int numero)
    {
        var endereçoEncontrado = await _endereçoRepository.GetByCep(cep, numero);
        return _mapper.Map<EndereçoResponse>(endereçoEncontrado);
    }
    public async Task<EndereçoResponse> Put(EndereçoRequest endereçoRequest, int? id) 
    {
        var endereçoDatabase = await _endereçoRepository.GetById((int)id);
        endereçoDatabase.Rua = endereçoRequest.Rua;
        var endereçoEditado = await _endereçoRepository.Put(endereçoDatabase, null);
        return _mapper.Map<EndereçoResponse>(endereçoEditado);
    }
    public async Task Delete(int id) 
    {
        var endereçoDatabase = await _endereçoRepository.GetById(id);
        if(endereçoDatabase != null)
        {
            await _endereçoRepository.Delete(endereçoDatabase);
        }
    }

    public async Task Delete(EndereçoRequest endereçoRequest)
    {
        throw new NotImplementedException();
    }
    
}