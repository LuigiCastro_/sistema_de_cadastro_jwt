using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_domain_contracts;
using AutoMapper;

namespace sistema_de_cadastro_crosscutting_mappers;
public class EndereçoEntitiesToContractMap : Profile
{

    // CRIANDO AUTOMAPPER ENTRE ENTITY E REQUEST/RESPONSE
    public EndereçoEntitiesToContractMap()
    {
        CreateMap<EndereçoEntities, EndereçoRequest>().ReverseMap();
        CreateMap<EndereçoEntities, EndereçoResponse>().ReverseMap();
    }       
}