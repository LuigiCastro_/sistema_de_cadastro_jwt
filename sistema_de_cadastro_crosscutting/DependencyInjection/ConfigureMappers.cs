using Microsoft.Extensions.DependencyInjection;
using sistema_de_cadastro_crosscutting_mappers;

namespace sistema_de_cadastro_crosscutting;
public class ConfigureMappers
{

    // CONFIGURANDO AUTOMAPPER ENTRE ENTITIES E CONTRACTS
    public static void ConfigureDependenciesMappers(IServiceCollection serviceCollection)
    {
        var config = new AutoMapper.MapperConfiguration(cfg => 
        {
            cfg.AddProfile(new EmpresaEntitiesToContractMap());
            cfg.AddProfile(new EndereçoEntitiesToContractMap());
            cfg.AddProfile(new UsuarioEntitiesToContractMap());
        });

        var mapConfiguration = config.CreateMapper();
        serviceCollection.AddSingleton(mapConfiguration);
    }
}