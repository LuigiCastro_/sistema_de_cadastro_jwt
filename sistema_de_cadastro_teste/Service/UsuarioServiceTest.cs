using sistema_de_cadastro_teste_crosscutting;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using sistema_de_cadastro_teste_faker;
using sistema_de_cadastro_repository;
using sistema_de_cadastro_service;
using AutoMapper;
using Xunit;
using Bogus;
using Moq;
using AutoMapper;

namespace sistema_de_cadastro_teste_service;

public class UsuarioServiceTeste
{
    private readonly Mock<IUsuarioRepository> _mockUsuarioRepository = new Mock<IUsuarioRepository>();
    private IMapper mapper = new AutoMapperFixture().GetMapper();
    private IServiceValidator _serviceValidator;


    [Fact(DisplayName = "Cadastra um novo usuário")]
    public async Task PostCadastroRequest()
    {
        var userRequest = UsuarioContractsFaker.UsuarioCadastroRequestNameFake();
        var userRequestEntities = UsuarioEntitiesFaker.UsuarioEntitiesNameFake();
        var resultUserRequest = UsuarioEntitiesFaker.UsuarioEntitiesNameFakeAsync(userRequestEntities.Nome);

        _mockUsuarioRepository.Setup(mock => mock.Post(It.IsAny<UsuarioEntities>())).Returns(resultUserRequest);
        // var teste = mapper.Map<UsuarioCadastroRequest>(userRequest);
        
        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);
        // var result = await service.PostCadastroRequest(teste);
        var result = await service.PostCadastroRequest(userRequest);

        Assert.Equal(result.Nome, resultUserRequest.Result.Nome);
    }


    [Fact(DisplayName = "Obtém uma lista de usuários")]
    public async Task Get()
    {
        _mockUsuarioRepository.Setup(mock => mock.Get()).Returns(UsuarioEntitiesFaker.UsuarioEntitiesListFakeAsync);
        
        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);
        var result = await service.Get();

        Assert.True(result.ToList().Count() > 0);
    }


    [Fact(DisplayName = "Obtém um usuário, via ID")]
    public async Task GetById()
    {
        int id = UsuarioEntitiesFaker.GetIdFake();
        _mockUsuarioRepository.Setup(mock => mock.GetById(id)).Returns(UsuarioEntitiesFaker.UsuarioEntitiesIdFakeAsync(id));

        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);
        var result = await service.GetById(id);

        Assert.Equal(result.Id, id);
    }


    [Fact(DisplayName = "Edita um dado de um usuário")]
    public async Task PutEmail()
    {
        var userRequest = UsuarioContractsFaker.UsuarioRequestEmailFake();
        var userRequestEntities = UsuarioEntitiesFaker.UsuarioEntitiesEmailFake();
        var resultUserRequest = UsuarioEntitiesFaker.UsuarioEntitiesEmailFakeAsync(userRequestEntities.Email);

        _mockUsuarioRepository.Setup(mock => mock.GetById(It.IsAny<int>())).Returns(resultUserRequest);
        _mockUsuarioRepository.Setup(mock => mock.Put(It.IsAny<UsuarioEntities>(), It.IsAny<int?>())).Returns(resultUserRequest);

        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);
        var result = await service.Put(userRequest, resultUserRequest.Result.Id);

        Assert.Equal(result.Email, resultUserRequest.Result.Email);
    }


    [Fact(DisplayName = "Remove um usuário")]
    public async Task Delete()
    {
        int id = UsuarioEntitiesFaker.GetIdFake();

        _mockUsuarioRepository.Setup(mock => mock.Delete(id)).Returns(() => Task.FromResult(string.Empty));

        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);

        try
        {
            await service.Delete(id);
        }
        catch (System.Exception)
        {
            Assert.True(false);
        }
    }


    [Fact(DisplayName = "Obtém um usuário, via CPF")]
    public async Task GetByCpf()
    {
        string cpf = UsuarioEntitiesFaker.GetCPFFake();
        _mockUsuarioRepository.Setup(mock => mock.GetByCpf(cpf)).Returns(UsuarioEntitiesFaker.UsuarioEntitiesCPFFakeAsync(cpf));

        var service = new UsuarioService(_mockUsuarioRepository.Object, mapper);
        var result = await service.GetByCpf(cpf);

        Assert.Equal(result.CPF, cpf);
    }
   
}
