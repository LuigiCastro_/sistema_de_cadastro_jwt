using sistema_de_cadastro_repository_context;
using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository;

// REPOSITORY É O BANCO DE DADOS
public class EmpresaRepository : IEmpresaRepository
{
    private readonly Context _context;      //  DbContext é quem permite o acesso aos métodos: ADD, FIND, EDIT, REMOVE, SAVE...
    public EmpresaRepository(Context context)
    {
        _context = context;
    }
    public async Task<EmpresaEntities> Post(EmpresaEntities empresaEntities) 
    {
        await _context.AddAsync(empresaEntities);
        await _context.SaveChangesAsync();
        return empresaEntities;
    }
    public async Task<IEnumerable<EmpresaEntities>> Get() 
    {
        // return await _context.Empresas.ToListAsync();
        return await _context.Empresas.Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .ToListAsync();
    }
    public async Task<EmpresaEntities> GetById(int id) 
    {
        // return await _context.Set<EmpresaEntities>().FindAsync(id);
        // return await _context.Empresas.FindAsync(id);
        // return await _context.Empresas.Where(prop => prop.Id == id).FirstOrDefaultAsync();
        return await _context.Empresas.Where(prop => prop.Id == id)
                                    .Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
    }
    public async Task<EmpresaEntities> GetByCnpj(string cnpj)
    {
        return await _context.Empresas.Where(prop => prop.Cnpj == cnpj)
                                    .Include(prop => prop.Endereço)
                                    .AsNoTracking()
                                    .FirstOrDefaultAsync();
    }
    public async Task<EmpresaEntities> Put(EmpresaEntities empresaEntities, int? id) 
    {
        _context.Empresas.Update(empresaEntities);
        await _context.SaveChangesAsync();
        return empresaEntities;
    }
    public async Task Delete(int id) 
    {
        throw new NotImplementedException();
    }

    public async Task Delete(EmpresaEntities empresaEntities)
    {
        _context.Empresas.Remove(empresaEntities);
        await _context.SaveChangesAsync();
    }
}