using sistema_de_cadastro_domain_interfaces;
using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;
using AutoMapper;

namespace sistema_de_cadastro_service;

// SERVICE É A CAMADA DE NEGOCIOS
public class EmpresaService : IEmpresaService
{
    private readonly IEmpresaRepository _empresaRepository;
    private readonly IMapper _mapper;
    public EmpresaService(IEmpresaRepository empresaRepository, IMapper mapper)
    {
        _empresaRepository = empresaRepository;
        _mapper = mapper;
    }
    public async Task<EmpresaResponse> Post(EmpresaRequest empresaRequest) 
    {
        var requestEmpresaEntities = _mapper.Map<EmpresaEntities>(empresaRequest);
        var empresaCadastrada = await _empresaRepository.Post(requestEmpresaEntities);
        return _mapper.Map<EmpresaResponse>(empresaCadastrada);
    }
    public async Task<IEnumerable<EmpresaResponse>> Get() 
    {
        var listaEmpresasDatabase = await _empresaRepository.Get();
        return _mapper.Map<IEnumerable<EmpresaResponse>>(listaEmpresasDatabase);
    }
    public async Task<EmpresaResponse> GetById(int id) 
    {
        var empresaEncontrada = await _empresaRepository.GetById(id);
        return _mapper.Map<EmpresaResponse>(empresaEncontrada);
    }
    public async Task<EmpresaResponse> GetByCnpj(string cnpj)
    {
        var empresaEncontrada = await _empresaRepository.GetByCnpj(cnpj);
        return _mapper.Map<EmpresaResponse>(empresaEncontrada);
    }
    public async Task<EmpresaResponse> Put(EmpresaRequest empresaRequest, int? id) 
    {
        var empresaDatabase = await _empresaRepository.GetById((int)id);
        empresaDatabase.Nome = empresaRequest.Nome;
        var empresaEditada = await _empresaRepository.Put(empresaDatabase, null);
        return _mapper.Map<EmpresaResponse>(empresaEditada);
    }
    public async Task Delete(int id) 
    {
        var empresaDatabase = await _empresaRepository.GetById(id);
        if(empresaDatabase != null)
        {
            await _empresaRepository.Delete(empresaDatabase);
        }
    }

    public async Task Delete(EmpresaRequest empresaRequest)
    {
        throw new NotImplementedException();
    }
    
}