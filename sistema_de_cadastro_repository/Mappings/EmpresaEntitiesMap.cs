using Microsoft.EntityFrameworkCore.Metadata.Builders;
using sistema_de_cadastro_domain_entities;
using Microsoft.EntityFrameworkCore;

namespace sistema_de_cadastro_repository_mappings;

public class EmpresaEntitiesMap : IEntityTypeConfiguration<EmpresaEntities>
{
    public void Configure(EntityTypeBuilder<EmpresaEntities> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).HasColumnName("id");
        builder.Property(x => x.Ativo).HasColumnName("ativo");
        builder.Property(x => x.DataCriaçao).HasColumnName("dataCriaçao");
        builder.Property(x => x.DataAlteraçao).HasColumnName("dataAlteraçao");
        builder.Property(x => x.Nome).HasColumnName("nome");
        builder.Property(x => x.NomeFantasia).HasColumnName("nomeFantasia");
        builder.Property(x => x.Cnpj).HasColumnName("cnpj");
        builder.Property(x => x.EndereçoId).HasColumnName("endereçoId");
        builder.ToTable("empresas");
    }
}