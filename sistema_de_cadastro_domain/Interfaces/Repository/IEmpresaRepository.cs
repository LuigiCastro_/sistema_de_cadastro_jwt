using sistema_de_cadastro_domain_entities;

namespace sistema_de_cadastro_domain_interfaces;
public interface IEmpresaRepository : IBaseCRUD<EmpresaEntities, EmpresaEntities>
{
    Task<EmpresaEntities> GetByCnpj(string cnpj);
    Task Delete(EmpresaEntities empresaEntities);
}