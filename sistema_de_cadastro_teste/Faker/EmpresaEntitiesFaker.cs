using sistema_de_cadastro_domain_entities;
using Bogus.Extensions.Brazil;
using Bogus;

namespace sistema_de_cadastro_teste_faker;

public class EmpresaEntitiesFaker
{
    private static readonly Faker Fake = new Faker();


    public static int GetIdFake()
    {
        return Fake.IndexFaker;
    }
    public static string GetCNPJFake()
    {
        return Fake.Company.Cnpj();
    }

    public static async Task<IEnumerable<EmpresaEntities>> EmpresaEntitiesListFakeAsync()
    {
        var minhaLista = new List<EmpresaEntities>();

        for (int i = 0; i < 5; i++)
        {
            minhaLista.Add(new EmpresaEntities()
            {
                Id = i,
                Nome = Fake.Name.FirstName(),
                NomeFantasia = Fake.Name.JobTitle(),
                Cnpj = Fake.Company.Cnpj(),
                // Endereço
            });
        }

        return minhaLista;
    }


    public static EmpresaEntities EmpresaEntitiesNameFake()
    {
        return new EmpresaEntities
        {
            Nome = Fake.Name.FirstName()
        };
    }

    public static async Task<EmpresaEntities> EmpresaEntitiesIdFakeAsync(int id)
    {
        return new EmpresaEntities()
        {
            Id = id,
            Nome = Fake.Name.FirstName()
        };
    }
    public static async Task<EmpresaEntities> EmpresaEntitiesNameFakeAsync(string nome)
    {
        return new EmpresaEntities()
        {
            Id = Fake.IndexFaker,
            Nome = nome
        };
    }
    
    public static async Task<EmpresaEntities> EmpresaEntitiesCNPJFakeAsync(string cnpj)
    {
        return new EmpresaEntities()
        {
            Id = Fake.IndexFaker,
            Cnpj = cnpj
        };
    }
}