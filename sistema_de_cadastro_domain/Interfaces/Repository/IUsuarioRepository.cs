using sistema_de_cadastro_domain_contracts;
using sistema_de_cadastro_domain_entities;

namespace sistema_de_cadastro_domain_interfaces;
public interface IUsuarioRepository : IBaseCRUD<UsuarioEntities, UsuarioEntities>
{
    Task<UsuarioEntities> GetByCpf(string cpf);
    Task<UsuarioEntities> GetByEmail(string email);
    Task<IEnumerable<UsuarioEntities>> GetWithoutCpf();
    Task Delete(UsuarioEntities usuarioEntities);

}